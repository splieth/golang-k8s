# golang-k8s


## Part 1
Write a web service in Go that reads from / writes to a Postgres database.

Implement a REST endpoint `/deployment` that

1) inserts a new object ("deployment") on a `POST` request and
2) reads one random entry from the database on a `GET` request.

The deployment table and therefore the corresponding object in Go may look like this:

```sql
CREATE SEQUENCE IF NOT EXISTS deployment_id_seq;
CREATE TABLE "deployment" (
    "id" int4 NOT NULL DEFAULT nextval('deployment_id_seq'::regclass),
    "component" varchar(255) NOT NULL,
    "person" varchar(255) NOT NULL,
    PRIMARY KEY ("id")
);
```

Make sure all settings are configurable via environment variables. If mandatory parameters are missing, make sure that the application fails fast. You might want to back that with unit tests.

Add a `Makefile` that handles running tests, building the binary file, starting the Postgres container, ...

### Hints
- Postgres driver for Go: [pq](https://github.com/lib/pq).
- Stick to Go's http router, you won't need gorilla or something else.
- For all external dependencies, stick to [go modules](https://blog.golang.org/using-go-modules).


## Part 2
Dockerize your Go web service using a multi stage build. Make sure that the size of the resulting docker image is as small as possible.

Make sure to add a task for the docker build to the Makefile.
